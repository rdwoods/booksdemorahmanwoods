package com.possibledemo.rwoods.booksrahmanwoods.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.possibledemo.rwoods.booksrahmanwoods.BuildConfig;

import java.io.IOException;
import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 *  Created by rwoods
 */

public class EbayBooksApiRestClient {

    private static EbayBooksService ebayBooksService;

    private static Retrofit retrofit;

    static {
        setupRestClient();
    }

    private EbayBooksApiRestClient() {
    }

    private static void setupRestClient() {

        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithoutExposeAnnotation();
        Gson gson = builder.create();

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request original = chain.request();
                        Request.Builder requestBuilder = original.newBuilder()
                                .header("Content-Type","application/json")
                                .header("Accept","*/*")
                                .method(original.method(), original.body());


                        Request request = requestBuilder.build();
                        return chain.proceed(request);
                    }
                })
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.ENV)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();

        ebayBooksService = retrofit.create(EbayBooksService.class);
    }

    public static EbayBooksService getApiClient() {
        // Return the synchronous HTTP client when the thread is not prepared
        return ebayBooksService;
    }

    public static Retrofit getRetrofit() {
        return retrofit;
    }
}