package com.possibledemo.rwoods.booksrahmanwoods.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.possibledemo.rwoods.booksrahmanwoods.R;
import com.possibledemo.rwoods.booksrahmanwoods.model.Book;
import java.util.ArrayList;


public class BooksRecyclerViewAdapter extends RecyclerView.Adapter<BooksRecyclerViewAdapter.ViewHolder> {

    private ArrayList<Book> mBooks;
    private Context mContext;

    public BooksRecyclerViewAdapter(Context context, ArrayList<Book> boards){
        this.mBooks = boards;
        this.mContext = context;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView ivBookCover;
        TextView tvBookTitle;
        TextView tvBookAuthor;

        ViewHolder(View v){
            super(v);

            ivBookCover = (ImageView) v.findViewById(R.id.iv_book_cover);
            tvBookTitle = (TextView) v.findViewById(R.id.tv_book_title);
            tvBookAuthor = (TextView) v.findViewById(R.id.tv_book_author);
        }
    }

    @Override
    public BooksRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.book_item_row, parent, false);

        BooksRecyclerViewAdapter.ViewHolder viewHolder = new BooksRecyclerViewAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position){
        Glide.with(mContext)
                .load(mBooks.get(position).getImageURL())
                .placeholder(R.mipmap.ic_launcher)
                .into(viewHolder.ivBookCover);
        viewHolder.tvBookTitle.setText(String.valueOf(mBooks.get(position).getTitle()));

        if (mBooks.get(position).getAuthor() != null) {
            viewHolder.tvBookAuthor.setText(String.valueOf(mBooks.get(position).getAuthor()));
        }
    }


    @Override
    public int getItemCount(){

        return mBooks.size();
    }

    public void clear(){
        int size = mBooks.size();
        mBooks.clear();
        notifyItemRangeRemoved(0, size);
    }
}