package com.possibledemo.rwoods.booksrahmanwoods.api;

import com.possibledemo.rwoods.booksrahmanwoods.model.Book;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;


public interface EbayBooksService {

    @GET("books.json")
    Call<List<Book>> getBooks();
}
