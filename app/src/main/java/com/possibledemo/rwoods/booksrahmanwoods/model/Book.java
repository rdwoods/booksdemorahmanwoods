package com.possibledemo.rwoods.booksrahmanwoods.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Book {

	@SerializedName("author")
	@Expose
	private String author;

	@SerializedName("imageURL")
	@Expose
	private String imageURL;

	@SerializedName("title")
	@Expose
	private String title;

	public void setAuthor(String author){
		this.author = author;
	}

	public String getAuthor(){
		return author;
	}

	public void setImageURL(String imageURL){
		this.imageURL = imageURL;
	}

	public String getImageURL(){
		return imageURL;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	@Override
 	public String toString(){
		return 
			"Book{" + 
			"author = '" + author + '\'' + 
			",imageURL = '" + imageURL + '\'' + 
			",title = '" + title + '\'' + 
			"}";
		}
}