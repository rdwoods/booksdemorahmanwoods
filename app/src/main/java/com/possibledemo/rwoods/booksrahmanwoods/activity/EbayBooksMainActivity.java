package com.possibledemo.rwoods.booksrahmanwoods.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.possibledemo.rwoods.booksrahmanwoods.R;
import com.possibledemo.rwoods.booksrahmanwoods.adapter.BooksRecyclerViewAdapter;
import com.possibledemo.rwoods.booksrahmanwoods.api.EbayBooksApiRestClient;
import com.possibledemo.rwoods.booksrahmanwoods.application.EbayBooksDemoApplication;
import com.possibledemo.rwoods.booksrahmanwoods.model.Book;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class EbayBooksMainActivity extends AppCompatActivity {

    @BindView(R.id.rv_books)
    RecyclerView rvBooks;

    @BindView(R.id.tv_no_network)
    TextView tvNoNetwork;

    private String TAG = EbayBooksMainActivity.this.getClass().getSimpleName();
    private Context mContext;
    private ArrayList<Book> mBooks;
    private BooksRecyclerViewAdapter mBooksRecyclerViewAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ebay_books_main);
        ButterKnife.bind(this);

        mContext = this;

        RecyclerView.LayoutManager recyclerViewLayoutManager = new LinearLayoutManager(mContext);

        rvBooks.setLayoutManager(recyclerViewLayoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvBooks.getContext(),
                ((LinearLayoutManager) recyclerViewLayoutManager).getOrientation());
        rvBooks.addItemDecoration(dividerItemDecoration);

        mBooks = new ArrayList<>();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (EbayBooksDemoApplication.hasNetworkConnection()) {

            Call<List<Book>> call = EbayBooksApiRestClient.getApiClient().getBooks();

            call.enqueue(new Callback<List<Book>>() {
                @Override
                public void onResponse(Call<List<Book>> call, Response<List<Book>> response) {
                    try {
                        mBooks = (ArrayList<Book>) response.body();

                        mBooksRecyclerViewAdapter = new BooksRecyclerViewAdapter(mContext, mBooks);

                        rvBooks.setAdapter(mBooksRecyclerViewAdapter);

                        rvBooks.setVisibility(View.VISIBLE);
                        tvNoNetwork.setVisibility(View.GONE);

                        Timber.d(TAG, "Retrieving book details");

                    } catch (Exception e) {
                        Timber.d(TAG, "Retrieving book details exception:: " + response.body());
                    }
                }

                @Override
                public void onFailure(Call<List<Book>> call, Throwable t) {
                    Timber.d(TAG, "Retrieving book details failure " + t.getMessage());
                }
            });
        } else {
            rvBooks.setVisibility(View.VISIBLE);
            tvNoNetwork.setVisibility(View.VISIBLE);
        }
    }
}
