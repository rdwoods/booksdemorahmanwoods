package com.possibledemo.rwoods.booksrahmanwoods.model;

import org.junit.Test;

import java.util.Random;
import java.util.UUID;

import static org.junit.Assert.*;

/**
 * Created by rahmanwoods on 4/16/17.
 */
public class BookTest {

    private Book testBook;

    @Test
    public void getAndSetAuthor() throws Exception {
        String author = UUID.randomUUID().toString();

        testBook.setAuthor(author);

        assertEquals(author, testBook.getAuthor());
    }

    @Test
    public void getAndSetImageURL() throws Exception {
        String imageUrl = UUID.randomUUID().toString();

        testBook.setImageURL(imageUrl);

        assertEquals(imageUrl, testBook.getImageURL());
    }

    @Test
    public void getAndSetTitle() throws Exception {
        String title = UUID.randomUUID().toString();

        testBook.setTitle(title);

        assertEquals(title, testBook.getTitle());
    }

}